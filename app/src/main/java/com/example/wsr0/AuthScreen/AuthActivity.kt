package com.example.wsr0.AuthScreen

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wsr0.MainScreen.MainActivity
import com.example.wsr0.R
import com.example.wsr0.RegScreen.RegActivity
import com.example.wsr0.common.*
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        auth.setOnClickListener {
            val username = username_auth.text.toString()
            val password = password_auth.text.toString()
            if (username.isNotEmpty() && password.isNotEmpty()){
                auth(this, username, password, object: OnComplete {
                    override fun onGood(modelAnswer: ModelAnswer) {
                        startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                        finish()
                    }
                })
            }else{
                showAlertDialog(this, "Ошибка", "Заполните все поля")
            }
        }

        to_reg.setOnClickListener {
            startActivity(Intent(this@AuthActivity, RegActivity::class.java))
        }
    }
}