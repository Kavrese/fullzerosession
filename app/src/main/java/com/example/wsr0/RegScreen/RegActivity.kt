package com.example.wsr0.RegScreen

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wsr0.AuthScreen.AuthActivity
import com.example.wsr0.MainScreen.MainActivity
import com.example.wsr0.R
import com.example.wsr0.common.*
import kotlinx.android.synthetic.main.activity_reg.*

class RegActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg)

        reg.setOnClickListener {
            val email = email_reg.text.toString()
            val username = username_reg.text.toString()
            val password = password_reg_1.text.toString()
            val password_2 = password_reg_2.text.toString()
            if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty()){
                if (validateEmail(email)){
                    if (password == password_2)
                        reg(this, username, email, password, object: OnComplete {
                            override fun onGood(modelAnswer: ModelAnswer) {
                                startActivity(Intent(this@RegActivity, MainActivity::class.java))
                                finish()
                            }
                        })
                    else
                        showAlertDialog(this, "Ошибка", "Пароли не совпадают")
                }else{
                    showAlertDialog(this, "Ошибка", "Не корректная почта")
                }
            }else{
                showAlertDialog(this, "Ошибка", "Заполните все поля")
            }
        }

        to_auth.setOnClickListener {
            startActivity(Intent(this, AuthActivity::class.java))
        }
    }
}