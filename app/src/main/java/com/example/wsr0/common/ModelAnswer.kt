package com.example.wsr0.common

data class ModelAnswer(
    val notice: Notice
)

data class Notice(
    val answer: String? = null,
    val error: String? = null,
    val token: String? = null,
    val text: String? = null
)
