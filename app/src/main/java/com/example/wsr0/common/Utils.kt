package com.example.wsr0.common

import android.app.AlertDialog
import android.content.Context
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun initRetrofit(): Api {
    return Retrofit.Builder()
        .baseUrl("http://cars.areas.su/")
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(Api::class.java)
}

fun showAlertDialog(context: Context, title: String, message: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton("OK", null)
        .show()
}

fun showAlertDialog(context: Context, title: String, t: Throwable){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(t.message)
        .setNegativeButton("OK", null)
        .show()
}

fun showAlertDialog(context: Context, title: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage("Повторите попытку позже !")
        .setNegativeButton("OK", null)
        .show()
}

fun validateEmail(email: String): Boolean{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun auth(context: Context, username: String, password: String, onComplete: OnComplete){
    initRetrofit().auth(username, password).enqueue(object: Callback<ModelAnswer>{

        override fun onResponse(call: Call<ModelAnswer>, response: Response<ModelAnswer>) {
            if (response.body() != null){
                val notice = response.body()!!.notice
                if (notice.token != null){
                    Info.token = notice.token
                    onComplete.onGood(response.body()!!)
                }else{
                    if (notice.answer != null && notice.answer == "User is active")
                        showAlertDialog(context, "Ошибка авторизации", "Пользователь уже зашёл в систему")
                    else {
                        if (notice.error != null){
                            showAlertDialog(context, "Ошибка авторизации", notice.error)
                        }else{
                            if (notice.answer != null){
                                showAlertDialog(context, "Ошибка авторизации", notice.answer)
                            }else {
                                showAlertDialog(context, "Ошибка авторизации")
                            }
                        }
                    }
                }
            }else{
                showAlertDialog(context, "Ошибка авторизации")
            }
        }

        override fun onFailure(call: Call<ModelAnswer>, t: Throwable) {
            showAlertDialog(context, "Ошибка авторизации", t)
        }
    })
}

fun reg(context: Context, username: String, email: String, password: String, onComplete: OnComplete){
    initRetrofit().reg(username, email, password).enqueue(object: Callback<ModelAnswer>{

        override fun onResponse(call: Call<ModelAnswer>, response: Response<ModelAnswer>) {
            if (response.body() != null){
                val notice = response.body()!!.notice
                if (notice.answer != null && notice.answer == "Success"){
                    auth(context, username, password, onComplete)
                }else{
                    if (notice.error != null){
                        showAlertDialog(context, "Ошибка регистрации", notice.error)
                    }else{
                        showAlertDialog(context, "Ошибка регистрации")
                    }
                }
            }else{
                showAlertDialog(context, "Ошибка регистрации")
            }
        }

        override fun onFailure(call: Call<ModelAnswer>, t: Throwable) {
            showAlertDialog(context, "Ошибка регистрации", t)
        }
    })
}