package com.example.wsr0.common

import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {
    @POST("login")
    fun auth(@Query("username") username: String, @Query("password") password: String): Call<ModelAnswer>

    @POST("signup")
    fun reg(@Query("username") username: String, @Query("email") email: String, @Query("password") password: String): Call<ModelAnswer>


}