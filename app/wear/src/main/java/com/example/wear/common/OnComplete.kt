package com.example.wsr0.common

interface OnComplete {
    fun onGood(modelAnswer: ModelAnswer)
}